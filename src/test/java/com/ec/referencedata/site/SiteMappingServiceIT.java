
package com.ec.referencedata.site;

import com.ec.referencedata.store.StoreMapping;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/referencedata-site-config.xml" })
@ActiveProfiles("production")
public class SiteMappingServiceIT {

    @Autowired
    private SiteMappingService siteMappingService;

    @Test
    public void testFindByStore() throws Exception {
        StoreMapping storeMapping = siteMappingService.getStoreMappingByURL("rspoland.com");
        Assert.assertEquals("pl", storeMapping.getLocale());
        Assert.assertEquals("PL_1", storeMapping.getStoreId());
        Assert.assertEquals("PL1", storeMapping.getSalesLocation());
        Assert.assertEquals("pl", storeMapping.getLanguageCode());
        Assert.assertEquals("PL", storeMapping.getCountryIsoCode());
        Assert.assertEquals("pl", storeMapping.getLabelLanguage());
        Assert.assertEquals("PLN", storeMapping.getCurrencyCode());
        Assert.assertEquals("pl", storeMapping.getPublicationLanguage());
        System.out.println(storeMapping);
    }
}
