
package com.ec.referencedata.site;

import com.ec.referencedata.store.StoreMapping;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/referencedata-site-config.xml" })
@ActiveProfiles("static1")
public class SiteMappingServiceITStatic1 {

    @Autowired
    private SiteMappingService siteMappingService;

    @Test
    public void testFindByStore() throws Exception {
        StoreMapping storeMapping = siteMappingService.getStoreMappingByURL("st1-f1.rs-online.com");
        Assert.assertEquals("f1", storeMapping.getLocale());
        Assert.assertEquals("FR_1", storeMapping.getStoreId());
        Assert.assertEquals("FR1", storeMapping.getSalesLocation());
        Assert.assertEquals("fr", storeMapping.getLanguageCode());
        Assert.assertEquals("FR", storeMapping.getCountryIsoCode());
        Assert.assertEquals("f1", storeMapping.getLabelLanguage());
        Assert.assertEquals("EUR", storeMapping.getCurrencyCode());
        Assert.assertEquals("fr", storeMapping.getPublicationLanguage());
        System.out.println(storeMapping);
    }
}
