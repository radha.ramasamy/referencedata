
package com.ec.referencedata.store;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/referencedata-store-context.xml" })
public class StoreMappingServiceIT {

    @Autowired
    private StoreMappingService service;

    @Test
    public void testFindByStore() throws Exception {
        final StoreMapping store = service.getMappingByStore("GB_1");
        Assert.assertNotNull(store);
    }

    @Test
    public void testFindByStoreAndLanguage() throws Exception {
        final StoreMapping store = service.getMappingByStoreAndLanguage("BE_1", "fr");
        Assert.assertNotNull(store);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByStoreAndLanguageNoFoundDueToStore() throws Exception {
        service.getMappingByStoreAndLanguage("test", "en");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByStoreAndLanguageDueToLanguage() throws Exception {
        service.getMappingByStoreAndLanguage("BE_1", "xx");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByStoreNoFound() throws Exception {
        service.getMappingByStore("test");
    }

    @Test
    public void testFindByLocale() throws Exception {
        final StoreMapping store = service.getMappingByLocale("uk");
        Assert.assertNotNull(store);
    }

    @Test
    public void testFindByLocaleCH() throws Exception {
        final StoreMapping store = service.getMappingByLocale("dech");
        Assert.assertNotNull(store);
    }

    @Test
    public void testFindByLocalePT() throws Exception {
        final StoreMapping store = service.getMappingByLocale("pt");
        Assert.assertNotNull(store);
    }

    @Test
    public void testFindByLocaleKR() throws Exception {
        final StoreMapping store = service.getMappingByLocale("kr");
        Assert.assertNotNull(store);
    }
}
