
package com.ec.referencedata.store;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StoreMappingTest {

    @Test
    public void testConstruction() {
        final StoreMapping mapping = new StoreMapping("store", "lang", "curr", "locale", "countryIso", "salesOrg", "salesLoc", "labelLang", "pub",
                "pubLang", "site", "tz", RegionType.EMEA, "hreflang");

        assertEquals("store", mapping.getStoreId());
        assertEquals("lang", mapping.getLanguageCode());
        assertEquals("curr", mapping.getCurrencyCode());
        assertEquals("locale", mapping.getLocale());
        assertEquals("countryIso", mapping.getCountryIsoCode());
        assertEquals("salesOrg", mapping.getSalesOrg());
        assertEquals("salesLoc", mapping.getSalesLocation());
        assertEquals("labelLang", mapping.getLabelLanguage());
        assertEquals("pub", mapping.getPublication());
        assertEquals("pubLang", mapping.getPublicationLanguage());
        assertEquals("site", mapping.getSite());
        assertEquals("tz", mapping.getTimeZone());
        assertEquals(RegionType.EMEA, mapping.getRegion());
        assertEquals("hreflang", mapping.getHreflang());
        assertEquals("locale", mapping.getParentLocale());
    }
    
    @Test
    public void testConstructionForPiggyBackMarketsWithParentLocale() {
        final StoreMapping mapping = new StoreMapping("store", "lang", "curr", "piggyBackMarketLocale", "countryIso", "salesOrg", "salesLoc", "labelLang", "pub",
                "pubLang", "site", "tz", RegionType.EMEA, "hreflang", "parentLocale");

        assertEquals("store", mapping.getStoreId());
        assertEquals("lang", mapping.getLanguageCode());
        assertEquals("curr", mapping.getCurrencyCode());
        assertEquals("piggyBackMarketLocale", mapping.getLocale());
        assertEquals("countryIso", mapping.getCountryIsoCode());
        assertEquals("salesOrg", mapping.getSalesOrg());
        assertEquals("salesLoc", mapping.getSalesLocation());
        assertEquals("labelLang", mapping.getLabelLanguage());
        assertEquals("pub", mapping.getPublication());
        assertEquals("pubLang", mapping.getPublicationLanguage());
        assertEquals("site", mapping.getSite());
        assertEquals("tz", mapping.getTimeZone());
        assertEquals(RegionType.EMEA, mapping.getRegion());
        assertEquals("hreflang", mapping.getHreflang());
        assertEquals("parentLocale", mapping.getParentLocale());
    }
}
