
package com.ec.referencedata.crm.legacy;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

/**
 * <code>GenerateListMapping</code> generates the legacy large properties to our own format.
 * 
 * @author cedric
 */
public class GenerateListMapping {

    private final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

    private void generate(final String path) throws Exception {
        final Map<Integer, Set<String>> values = new TreeMap<>();
        final Pattern pattern = Pattern.compile("<codeA>(.+?)</codeA>\\s*<codeB>(.+?)</codeB>");
        final Resource[] resources = resolver.getResources(path);
        for (Resource resource : resources) {
            try {
                final String content = new String(Files.readAllBytes(Paths.get(resource.getURI())));
                final Matcher matcher = pattern.matcher(content);
                while (matcher.find()) {
                    final Integer key = Integer.valueOf(matcher.group(2));
                    final Set<String> mappings = values.getOrDefault(key, new HashSet<String>());
                    mappings.add(matcher.group(1));
                    values.put(key, mappings);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final StringBuilder sb = new StringBuilder();
        sb.append("<util:map key-type=\"java.lang.Integer\" value-type=\"java.lang.String\">");

        values.keySet().forEach(k -> {
            final Set<String> mappings = values.get(k);
            if (mappings.size() > 1) {
                System.err.println("multiple values for {key=" + k + "}");

            } else {
                sb.append("<entry key=\"");
                sb.append(k);
                sb.append("\" value=\"");
                sb.append(mappings.iterator().next());
                sb.append("\" />");
            }
        });

        sb.append("</util:map>");
        System.out.println(sb.toString());
    }

    @Test
    public void generateHearAbout() throws Exception {
        generateLocale("/legacy/hearabout/*.xml");
        Assert.assertTrue(true);
    }

    @Test
    public void generateJobFunction() throws Exception {
        generate("/legacy/func/*.xml");
        Assert.assertTrue(true);
    }
    
    @Test
    public void generateCountry() throws Exception {
        generateLocale("/legacy/country/*.xml");
        Assert.assertTrue(true);
    }

    @Test
    public void generateJobTech() throws Exception {
        generate("/legacy/tech/*.xml");
        Assert.assertTrue(true);
    }

    private void generateLocale(final String path) throws Exception {
        final Pattern pattern = Pattern.compile("<codeA>(.+?)</codeA>\\s*<codeB>(.+?)</codeB>");
        final Pattern localePattern = Pattern.compile("locale=\"(.+)\"");
        final Resource[] resources = resolver.getResources(path);

        final StringBuilder sb = new StringBuilder();
        sb.append("<util:map key-type=\"java.lang.String\" value-type=\"java.util.Map\">");

        for (Resource resource : resources) {
            try {
                final String content = new String(Files.readAllBytes(Paths.get(resource.getURI())));
                final Matcher matcher = pattern.matcher(content);
                final String locale = getLocale(localePattern, content);
                sb.append("<entry key=\"");
                sb.append(locale);
                sb.append("\">");
                sb.append("<util:map key-type=\"java.lang.Integer\" value-type=\"java.lang.String\">");
                while (matcher.find()) {
                    sb.append("<entry key=\"");
                    sb.append(matcher.group(2));
                    sb.append("\" value=\"");
                    sb.append(matcher.group(1));
                    sb.append("\" />");
                }
                sb.append("</util:map>");
                sb.append("</entry>");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sb.append("</util:map>");
        System.out.println(sb.toString());
    }

    @Test
    public void generatePrimaryInterest() throws Exception {
        generate("/legacy/prodint/*.xml");
        Assert.assertTrue(true);
    }

    @Test
    public void generateProfessionalTitles() throws Exception {
        generateLocale("/legacy/ptitle/*.xml");
        Assert.assertTrue(true);
    }

    @Test
    public void generatePurchasingInfluence() throws Exception {
        generate("/legacy/purch/*.xml");
        Assert.assertTrue(true);
    }

    @Test
    public void generateTitles() throws Exception {
        generate("/legacy/title/*.xml");
        Assert.assertTrue(true);
    }

    private String getLocale(final Pattern pattern, final String content) {
        final Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            return matcher.group(1);
        }
        return StringUtils.EMPTY;
    }

}
