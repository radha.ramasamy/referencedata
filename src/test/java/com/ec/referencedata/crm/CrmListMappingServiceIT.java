
package com.ec.referencedata.crm;

import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ec.referencedata.store.StoreMappingService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/referencedata-crm-list-context.xml", "/referencedata-store-context.xml" })
public class CrmListMappingServiceIT {

    @Autowired
    private CrmListMappingService service;

    @Autowired
    private StoreMappingService storeMappingService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testCountry() throws Exception {
        final Set<String> locales = storeMappingService.getAllMappings().stream().map(m -> m.getLocale()).collect(Collectors.toSet());
        locales.forEach(l -> {
            String key = l.toUpperCase();
            if (key.equals("DECH")) {
                key = "CH";
                
            } else if (key.equals("KR")) {
                key = "HK";
                
            } else if (key.startsWith("BE")) {
                key = "BE";
                
            } else if (key.startsWith("TW")) {
                key = "TW";
                
            } else if (key.startsWith("HK")) {
                key = "HK";
            }
            final String value = service.getListValue(l, CrmListType.COUNTRY, key);
            Assert.assertTrue(StringUtils.isNotBlank(value));
        });
    }

    @Test
    public void testHearAbout() throws Exception {
        final Set<String> locales = storeMappingService.getAllMappings().stream().map(m -> m.getLocale()).collect(Collectors.toSet());
        locales.forEach(l -> {
            final String value = service.getListValue(l, CrmListType.HEAR_ABOUT, "1");
            Assert.assertTrue(StringUtils.isNotBlank(value));
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHearAboutNotFound() throws Exception {
        service.getListValue("test", CrmListType.HEAR_ABOUT, "1");
    }

    @Test
    public void testJobFunc() throws Exception {
        final Set<String> locales = storeMappingService.getAllMappings().stream().map(m -> m.getLocale()).collect(Collectors.toSet());
        locales.forEach(l -> {
            final String value = service.getListValue(l, CrmListType.JOB_FUNC, "1");
            Assert.assertTrue(StringUtils.isNotBlank(value));
        });
    }

    @Test
    public void testJobTech() throws Exception {
        final Set<String> locales = storeMappingService.getAllMappings().stream().map(m -> m.getLocale()).collect(Collectors.toSet());
        locales.forEach(l -> {
            final String value = service.getListValue(l, CrmListType.JOB_TECH, "2");
            Assert.assertTrue(StringUtils.isNotBlank(value));
        });
    }

    @Test
    public void testNullCrmListType_Throws() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        service.getListValue("uk", null, "1");
    }

    @Test
    public void testProfTitle() throws Exception {
        final Set<String> locales = storeMappingService.getAllMappings().stream().map(m -> m.getLocale()).collect(Collectors.toSet());
        locales.forEach(l -> {
            final String value = service.getListValue(l, CrmListType.PROF_TITLE, "3");
            Assert.assertTrue(StringUtils.isNotBlank(value));
        });
    }

    @Test
    public void testTitle() throws Exception {
        final Set<String> locales = storeMappingService.getAllMappings().stream().map(m -> m.getLocale()).collect(Collectors.toSet());
        locales.forEach(l -> {
            final String value = service.getListValue(l, CrmListType.TITLE, "1");
            Assert.assertTrue(StringUtils.isNotBlank(value));
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTitleNotFound() throws Exception {
        service.getListValue("test", CrmListType.TITLE, "0");
    }
}
