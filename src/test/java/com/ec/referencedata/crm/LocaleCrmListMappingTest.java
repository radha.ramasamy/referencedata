
package com.ec.referencedata.crm;

import java.util.HashMap;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class LocaleCrmListMappingTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void computeValue_NoValueForKey() {

        final Map<String, Map<String, String>> mappings = new HashMap<>();
        final LocaleCrmListMapping localeCrmListMapping = new LocaleCrmListMapping(mappings);

        thrown.expect(IllegalArgumentException.class);

        localeCrmListMapping.computeValue("locale", "1");
    }
}
