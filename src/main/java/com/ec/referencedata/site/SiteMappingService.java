
package com.ec.referencedata.site;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ec.referencedata.store.StoreMapping;
import com.ec.referencedata.store.StoreMappingService;

/**
 * Static data backed service for resolving site parameters for a given RS URL.
 */
public class SiteMappingService {
    private final Map<String, String> mappings;

    private final StoreMappingService storeMappingService;

    /**
     * Constructs a new SiteMappingService with the given parameters.
     *
     * @param storeMappingService store mapping service
     * @param mappings URL mappings
     */
    public SiteMappingService(final StoreMappingService storeMappingService, final Map<String, String> mappings) {
        this.storeMappingService = storeMappingService;
        this.mappings = mappings;
    }

    /**
     * Get the StoreMapping by URL (e.g. fr-rsonline.com).
     * 
     * @param url An RS domain URL
     * @return The StoreInformation for the requested URL
     */
    public StoreMapping getStoreMappingByURL(final String url) {
        return storeMappingService.getMappingByLocale(mappings.get(url));
    }

    /**
     * Get the StoreMapping by URL (e.g. fr-rsonline.com).
     * 
     * @param url An RS domain URL
     * @param defaultLocale A locale to default to if the requested URL could not be resolved
     * @return The StoreInformation for the requested URL
     */
    public StoreMapping getStoreMappingByURL(final String url, final String defaultLocale) {
        final String rsLocale = mappings.get(url);
        return storeMappingService.getMappingByLocale(StringUtils.isNotBlank(rsLocale) ? rsLocale : defaultLocale);
    }
}
