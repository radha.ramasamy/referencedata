
package com.ec.referencedata.store;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * <code>StoreMappingService</code> finds stores against different criteria.
 *
 * @author $author: cedric
 */
public class StoreMappingService {
    private final List<StoreMapping> mappings;

    /**
     * Constructs a new StoreMappingService with the given parameters.
     *
     * @param mappings - the mappings
     */
    public StoreMappingService(final List<StoreMapping> mappings) {
        this.mappings = mappings;
    }

    /**
     * Gets all the mappings.
     *
     * @return the mappings
     */
    public List<StoreMapping> getAllMappings() {
        return Collections.unmodifiableList(mappings);
    }

    /**
     * Search by locale.
     *
     * @param locale - the locale
     * @return the mapping
     */
    public StoreMapping getMappingByLocale(final String locale) {
        final Optional<StoreMapping> mapping = mappings.stream().filter(sm -> sm.getLocale().equalsIgnoreCase(locale)).findFirst();
        return mapping.map(sm -> sm).orElseThrow(() -> new IllegalArgumentException("invalid locale {" + locale + "}"));
    }

    /**
     * Search by store.
     *
     * @param store - the store
     * @return the mapping
     */
    public StoreMapping getMappingByStore(final String store) {
        final Optional<StoreMapping> mapping = mappings.stream().filter(sm -> sm.getStoreId().equalsIgnoreCase(store)).findFirst();
        return mapping.map(sm -> sm).orElseThrow(() -> new IllegalArgumentException("invalid store {" + store + "}"));
    }

    /**
     * Search by store and language.
     *
     * @param store - the store
     * @param language - the language
     * @return the mapping
     */
    public StoreMapping getMappingByStoreAndLanguage(final String store, final String language) {
        final Optional<StoreMapping> mapping = mappings.stream()
                .filter(sm -> sm.getStoreId().equalsIgnoreCase(store) && sm.getLanguageCode().equalsIgnoreCase(language)).findFirst();
        return mapping.map(sm -> sm).orElseThrow(() -> new IllegalArgumentException("invalid store {" + store + "} or language {" + language + "}"));
    }
}
