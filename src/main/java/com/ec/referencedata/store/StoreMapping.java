
package com.ec.referencedata.store;

/**
 * <code>StoreMapping</code> defines a store mapping.
 * 
 * @author cedric
 */
public class StoreMapping {

    private final String countryIsoCode;

    private final String labelLanguage;

    private final String languageCode;

    private final String locale;

    private final String publication;

    private final String publicationLanguage;

    private final String salesLocation;

    private final String salesOrg;

    private final String site;

    private final String storeId;

    private final String timeZone;

    private final String currencyCode;

    private final RegionType region;

    private final String parentLocale;

    private final String hreflang;

    /**
     * Constructs a new mappping
     * 
     * @param storeId - the store
     * @param languageCode - the language
     * @param currencyCode - the currency
     * @param locale - the locale
     * @param countryIsoCode - the country
     * @param salesOrg - the sales organisation
     * @param salesLocation - the sales location
     * @param labelLanguage - the label language
     * @param publication - the publication
     * @param publicationLanguage - the publication language
     * @param site - the site
     * @param timeZone - the time zone
     * @param region - the region
     * @param hreflang - the hreflang
     */
    public StoreMapping(final String storeId, final String languageCode, final String currencyCode, final String locale, final String countryIsoCode,
            final String salesOrg, final String salesLocation, final String labelLanguage, final String publication, final String publicationLanguage,
            final String site, final String timeZone, final RegionType region, final String hreflang) {

        this(storeId, languageCode, currencyCode, locale, countryIsoCode, salesOrg, salesLocation, //
                labelLanguage, publication, publicationLanguage, site, timeZone, region, hreflang, locale);
    }

    /**
     * Constructs a new mapping for locales with parents. Used for defaulting labels.
     * 
     * @param storeId - the store
     * @param languageCode - the language
     * @param currencyCode - the currency
     * @param locale - the locale
     * @param countryIsoCode - the country
     * @param salesOrg - the sales organisation
     * @param salesLocation - the sales location
     * @param labelLanguage - the label language
     * @param publication - the publication
     * @param publicationLanguage - the publication language
     * @param site - the site
     * @param timeZone - the time zone
     * @param region - the region
     * @param hreflang - the hreflang
     * @param parentLocale - The parent Locale to resolve message labels to.
     */
    public StoreMapping(final String storeId, final String languageCode, final String currencyCode, final String locale, final String countryIsoCode,
            final String salesOrg, final String salesLocation, final String labelLanguage, final String publication, final String publicationLanguage,
            final String site, final String timeZone, final RegionType region, final String hreflang, final String parentLocale) {

        this.storeId = storeId;
        this.languageCode = languageCode;
        this.currencyCode = currencyCode;
        this.locale = locale;
        this.countryIsoCode = countryIsoCode;
        this.salesOrg = salesOrg;
        this.salesLocation = salesLocation;
        this.labelLanguage = labelLanguage;
        this.publication = publication;
        this.publicationLanguage = publicationLanguage;
        this.site = site;
        this.timeZone = timeZone;
        this.region = region;
        this.hreflang = hreflang;
        this.parentLocale = parentLocale;
    }

    /**
     * Gets the region.
     *
     * @return the region
     */
    public RegionType getRegion() {
        return region;
    }

    /**
     * Gets the currency.
     * 
     * @return the currency
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Gets the country.
     * 
     * @return the country
     */
    public String getCountryIsoCode() {
        return countryIsoCode;
    }

    /**
     * Gets the language label.
     * 
     * @return the label
     */
    public String getLabelLanguage() {
        return labelLanguage;
    }

    /**
     * Gets the language.
     * 
     * @return the language
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * Gets the locale.
     * 
     * @return the locale
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Gets the publication
     * 
     * @return the publication
     */
    public String getPublication() {
        return publication;
    }

    /**
     * Gets the publication language.
     * 
     * @return the language
     */
    public String getPublicationLanguage() {
        return publicationLanguage;
    }

    /**
     * Gets the sales location.
     * 
     * @return the location
     */
    public String getSalesLocation() {
        return salesLocation;
    }

    /**
     * Gets the sales organisation.
     * 
     * @return the organisation
     */
    public String getSalesOrg() {
        return salesOrg;
    }

    /**
     * Gets the site.
     * 
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * Gets the store.
     * 
     * @return the store
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * Gets the time zone.
     * 
     * @return the time zone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Gets the parent locale for piggy back markets
     * 
     * @return the parent locale.
     */
    public String getParentLocale() {
        return parentLocale;
    }

    public String getHreflang() {
        return hreflang;
    }
}
