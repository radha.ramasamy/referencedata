
package com.ec.referencedata.store;

/**
 * <code>RegionType</code> defines the different region.
 *
 * @author $author: cedric
 */
public enum RegionType {
    APAC, //
    EMEA, //
}
