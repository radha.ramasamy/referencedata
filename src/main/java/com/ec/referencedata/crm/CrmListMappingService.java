
package com.ec.referencedata.crm;

import java.util.Map;

/**
 * <code>CrmListMappingService</code> provides a mapping for the list of values between the internal ID and the CRM code.
 * 
 * @author cedric
 */
public class CrmListMappingService {
    private final Map<CrmListType, CrmListMapping> mappings;

    /**
     * Constructs a new service.
     * 
     * @param mappings - the mappings
     */
    public CrmListMappingService(final Map<CrmListType, CrmListMapping> mappings) {
        this.mappings = mappings;
    }

    /**
     * Gets a value.
     * 
     * @param locale - the locale
     * @param type - the type
     * @param key - the key
     * @return the value
     */
    public String getListValue(final String locale, final CrmListType type, final String key) {
        final CrmListMapping mapping = mappings.get(type);
        if (mapping != null) {
            return mapping.computeValue(locale, key);
        }

        final String typeName = (type == null) ? null : type.name();
        throw new IllegalArgumentException("invalid list type {type=" + typeName + ", locale=" + locale + ", key=" + key + "}");
    }

    public String getListKey(final String locale, final CrmListType type, final String value) {
        final CrmListMapping mapping = mappings.get(type);
        if (mapping != null) {
            return mapping.computeKey(locale, value);
        }

        final String typeName = (type == null) ? null : type.name();
        throw new IllegalArgumentException("invalid list type {type=" + typeName + ", locale=" + locale + ", value=" + value + "}");
    }
}
