
package com.ec.referencedata.crm;

import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;

/**
 * <code>GlobalCrmListMapping</code> provides a global mapping.
 * 
 * @author cedric
 */
public class GlobalCrmListMapping implements CrmListMapping {
    private final Map<String, String> values;

    /**
     * Constructs a new mapping.
     * 
     * @param values - the values
     */
    public GlobalCrmListMapping(final Map<String, String> values) {
        this.values = values;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String computeValue(final String locale, final String key) {
        final String value = values.get(key);
        if (StringUtils.isNotBlank(value)) {
            return value;
        }
        throw new IllegalArgumentException("list value not found {locale=" + locale + ", key=" + key + "}");
    }

    @Override
    public String computeKey(final String locale, final String value) {
        final Optional<String> key = values.keySet().stream().filter(k -> values.get(k).equals(value)).findFirst();
        return key.map(k -> k).orElseThrow(() -> new IllegalArgumentException("list key not found {locale=" + locale + ", value=" + value + "}"));
    }

}
