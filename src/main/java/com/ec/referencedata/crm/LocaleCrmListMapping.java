
package com.ec.referencedata.crm;

import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

/**
 * <code>LocaleCrmListMapping</code> provides a localised mapping.
 * 
 * @author cedric
 */
public class LocaleCrmListMapping implements CrmListMapping {
    private final Map<String, Map<String, String>> locales;

    /**
     * Constructs a new mapping.
     * 
     * @param locales - the locales
     */
    public LocaleCrmListMapping(final Map<String, Map<String, String>> locales) {
        this.locales = locales;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String computeValue(final String locale, final String key) {
        final Map<String, String> values = locales.get(locale);
        if (!CollectionUtils.isEmpty(values)) {
            final String value = values.get(key);
            if (StringUtils.isNotBlank(value)) {
                return value;
            }
        }
        throw new IllegalArgumentException("list value not found {locale=" + locale + ", key=" + key + "}");
    }

    @Override
    public String computeKey(final String locale, final String value) {
        final Map<String, String> values = locales.get(locale);
        if (!CollectionUtils.isEmpty(values)) {
            final Optional<String> key = values.keySet().stream().filter(k -> values.get(k).equals(value)).findFirst();
            if (key.isPresent()) {
                return key.get();
            }
        }
        throw new IllegalArgumentException("list key not found {locale=" + locale + ", value=" + value + "}");
    }

}
