
package com.ec.referencedata.crm;

/**
 * <code>CrmListType</code> defines the different list of values.
 * 
 * @author cedric
 */
public enum CrmListType {
    HEAR_ABOUT, //
    JOB_FUNC, //
    JOB_TECH, //
    PRIMARY_INTEREST, //
    PROF_TITLE, //
    PURCHASING_INFLUENCE, //
    TITLE, //
    COUNTRY, //
}
