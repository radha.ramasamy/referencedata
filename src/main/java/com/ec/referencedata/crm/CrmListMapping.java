
package com.ec.referencedata.crm;

/**
 * <code>CrmListMapping</code> defines a way of computing a mapping.
 * 
 * @author cedric
 */
public interface CrmListMapping {

    /**
     * Gets a mapping value.
     * 
     * @param locale - the locale
     * @param key - the key
     * @return the mapped value
     */
    String computeValue(String locale, String key);

    /**
     * Gets a mapping key.
     *
     * @param locale locale
     * @param value value
     * @return the mapped key
     */
    String computeKey(String locale, String value);
}
